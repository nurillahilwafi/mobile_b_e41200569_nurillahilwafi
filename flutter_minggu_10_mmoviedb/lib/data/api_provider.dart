import 'dart:convert';

import 'package:flutter_minggu_10_mmoviedb/model/popular_movies.dart';
import 'package:http/http.dart' show Client, Response;


class ApiProvider {
  String apiKey = 'cf1b60731119e5a4c4ecd579507168da';
  String baseUrl = 'https://api.themoviedb.org/3';

  Client client = Client();

  Future<PopularMovies> getPopularMovies() async {
    // String url = '$baseUrl/movie/popular?api_key=$apiKey';
    // print(url);
    Response response =
    await client.get('$baseUrl/movie/popular?api_key=$apiKey');

    if (response.statusCode == 200) {
      return PopularMovies.fromJson(jsonDecode(response.body));
    } else {
      throw Exception(response.statusCode);
    }
  }
}